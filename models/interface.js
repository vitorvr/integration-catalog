const mongoose = require('mongoose');

const InterfaceSchema = mongoose.Schema({
  global_id: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  provider_app: [{
    type: String,
    required: true
  }],
  receiver_app: [{
    type: String,
    required: true
  }],
  status: {
    type: String,
    required: true
  },
  running_rule: {
    type: String,
    required: true
  },
  crossreference: [{
    type: String,
    required: true
  }],
  scheduler_type: {
    type: String,
    required: true
  },
  scheduler_chain: {
    type: String,
    required: false
  },
  deploy_name: {
    type: String,
    required: true
  },
  deploy_name: {
    type: String,
    required: true
  },
  deploy_location: [{
    type: String,
    required: true
  }],
  port: {
    type: Number,
    required: true
  },
  periodicity: {
    type: String,
    required: true
  },
  volume: {
    type: String,
    required: true
  },
  diagram: {
    type: String,
    required: true
  }
});

const Interface = module.exports = mongoose.model('Interface', InterfaceSchema);

module.exports.get = async (filters) => {
  try {
    let data = await Interface.find(filters);
    return { data: data, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.add = async (payload) => {
  try {
    let res = await Interface.create(payload);
    return { data: res, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.getById = async (id) => {
  try {
    let data = await Interface.findOne({ interface_id: id });
    return { data: data, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.delete = async (id) => {
  try {
    let data = await Interface.findByIdAndDelete(id);
    return { data: data, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.update = async (id, values) => {
  try {
    let interface = await Interface.findById(id);
    interface.global_id = values.global_id;
    interface.title = values.title;
    interface.description = values.description;
    interface.provider_app = values.provider_app;
    interface.receiver_app = values.receiver_app;
    interface.status = values.status;
    interface.running_rule = values.running_rule;
    interface.crossreference = values.crossreference;
    interface.scheduler_type = values.scheduler_type;
    interface.scheduler_chain = values.scheduler_chain;
    interface.deploy_name = values.deploy_name;
    interface.deploy_location = values.deploy_location;
    interface.port = values.port;
    interface.periodicity = values.periodicity;
    interface.volume = values.volume;
    interface.diagram = values.diagram;
    updatedInterface = await interface.save();
    return { data: updatedInterface, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};