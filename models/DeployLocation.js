const mongoose = require('mongoose');
const timestamp = require('mongoose-timestamp')

const DeployLocationScheme = mongoose.Schema({
  name: {
    type: String,
    required: true
  }
});

DeployLocationScheme.plugin(timestamp);

const DeployLocation = module.exports = mongoose.model('DeployLocation', DeployLocationScheme);

module.exports.get = async (filters) => {
  try {
    let data = await DeployLocation.find(filters);
    return { data: data, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.add = async (payload) => {
  try {
    let res = await DeployLocation.create(payload);
    return { data: res, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.getById = async (id) => {
  try {
    let data = await DeployLocation.findOne({ deployLocation_id: id });
    return { data: data, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.delete = async (id) => {
  try {
    let data = await DeployLocation.findByIdAndDelete(id);
    return { data: data, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.update = async (id, values) => {
  try {
    let deployLocation = await DeployLocation.findById(id);
    deployLocation.name = values.name;
    data = await deployLocation.save();
    return { data: data, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};