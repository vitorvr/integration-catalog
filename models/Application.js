const mongoose = require('mongoose');
const timestamp = require('mongoose-timestamp')

const ApplicationSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  }
});

ApplicationSchema.plugin(timestamp);

const Application = module.exports = mongoose.model('Application', ApplicationSchema);

module.exports.get = async (filters) => {
  try {
    let data = await Application.find(filters);
    return { data: data, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.add = async (payload) => {
  try {
    let res = await Application.create(payload);
    return { data: res, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.getById = async (id) => {
  try {
    let data = await Application.findOne({ application_id: id });
    return { data: data, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.delete = async (id) => {
  try {
    let data = await Application.findByIdAndDelete(id);
    return { data: data, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};

module.exports.update = async (id, values) => {
  try {
    let application = await Application.findById(id);
    application.name = values.name;
    application.description = values.description;
    data = await application.save();
    return { data: data, err: null };
  } catch (err) {
    return { data: null, err: err };
  }
};