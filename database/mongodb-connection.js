const mongoose = require('mongoose');

mongoose.Promise = Promise;

mongoose.connection.on('connected', () => {
  console.log('Database Connection Established');
});

mongoose.connection.on('reconnected', () => {
  console.log('Database Connection Reestablished');
});

mongoose.connection.on('disconnected', () => {
  console.log('Database Connection Disconnected');
});

mongoose.connection.on('close', () => {
  console.log('Database Connection Closed');
});

mongoose.connection.on('error', error => {
  console.log('ERROR: ' + error);
});

exports.run = async () => {
  await mongoose.connect(
    'mongodb://integrationcatalog-db:iS2dz4Kssy7y9oeEsBpfXwbMSeH34xOiMSKzoocKbdWDARhIU0yJ7a9wuMDs6ERMhWYHWOglsSnwRpZjiOGBZw==@integrationcatalog-db.documents.azure.com:10255/?ssl=true',
    //"mongodb://localhost:27017/integration-management",
    {
      autoReconnect: true,
      reconnectTries: 1000000,
      reconnectInterval: 3000,
      useNewUrlParser: true
    }
  );
};
