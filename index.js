const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongodb = require('./database/mongodb-connection.js');

const app = express();
const port = process.env.PORT || 3000;
const interfaces = require('./routes/api/interfaces');
const applications = require('./routes/api/applications');
const deploylocation = require('./routes/api/deploylocation');
const login = require('./routes/api/login');

app.use(bodyParser.json());
app.use(cors());
app.use('/api/interfaces', interfaces);
app.use('/api/applications', applications);
app.use('/api/deploylocation', deploylocation);
app.use('/api/login', login);

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(__dirname + '/public'));
  app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.html'));
}

mongodb.run().catch(error => console.error(error));
app.listen(port, () => console.log(`Server started on port ${port}`));
