const express = require('express');
const Application = require('../../models/Application');

const router = express.Router();

router.get('/', async (req, res, next) => {
  let response = await Application.get({});
  res.send(response.data);
  next();
});

router.post('/', async (req, res, next) => {
  let response = await Application.add(req.body);
  if (response.err) {
    res.status(500).send(response.err);
  } else {
    res.status(200).send(response.data);
  }
  next();
});

router.delete('/:id', async (req, res, next) => {
  let response = await Application.delete(req.params.id);
  if (response.err) {
    res.status(500).send(response.err);
  } else {
    res.status(200).send(response.data);
  }
  next();
});

router.put('/:id', async (req, res, next) => {
  let response = await Application.update(req.params.id, req.body);
  if (response.err) {
    res.status(500).send(response.err);
  } else {
    res.status(200).send(response.data);
  }
  next();
});

module.exports = router;