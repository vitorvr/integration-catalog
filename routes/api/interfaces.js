const express = require('express');
const db = require('../../models/Interface');

const router = express.Router();

router.get('/', async (req, res) => {
  let response = await db.get();
  res.send(response.data);
});

router.post('/', async (req, res) => {
  let response = await db.add(req.body);
  if (response.err) {
    res.status(500).send(response.err);
  } else {
    res.status(200).send(response.data);
  }
});

router.delete('/:id', async (req, res) => {
  let response = await db.delete(req.params.id);
  if (response.err) {
    res.status(500).send(response.err);
  } else {
    res.status(200).send(response.data);
  }
});

router.put('/:id', async (req, res) => {
  let response = await db.update(req.params.id, req.body);
  if (response.err) {
    res.status(500).send(response.err);
  } else {
    res.status(200).send(response.data);
  }
});

module.exports = router;
