const express = require('express');
const router = express.Router();

router.post('/', async (req, res, next) => {
  if (!(req.body.username === 'C0520007')) {
    res.status(401).send('Login Failed!');
  } else {
    res.status(200).send('Login Success!');
  }
  next();
});

module.exports = router;
