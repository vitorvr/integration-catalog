const express = require('express');
const DeployLocation = require('../../models/DeployLocation');

const router = express.Router();

router.get('/', async (req, res, next) => {
  let response = await DeployLocation.get({});
  res.send(response.data);
  next();
});

router.post('/', async (req, res, next) => {
  let response = await DeployLocation.add(req.body);
  if (response.err) {
    res.status(500).send(response.err);
  } else {
    res.status(200).send(response.data);
  }
  next();
});

router.delete('/:id', async (req, res, next) => {
  let response = await DeployLocation.delete(req.params.id);
  if (response.err) {
    res.status(500).send(response.err);
  } else {
    res.status(200).send(response.data);
  }
  next();
});

router.put('/:id', async (req, res, next) => {
  let response = await DeployLocation.update(req.params.id, req.body);
  if (response.err) {
    res.status(500).send(response.err);
  } else {
    res.status(200).send(response.data);
  }
  next();
});

module.exports = router;
