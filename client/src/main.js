import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/index';
import BootstrapVue from 'bootstrap-vue';
import Adal from 'vue-adal'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);
Vue.use(Adal, {
  config: {
    tenant: 'globalvale.onmicrosoft.com',
    clientId: '81fc528e-f35b-420c-9396-435b5eea927b',
    redirectUri: 'http://integrationcatalog-app.azurewebsites.net/',
    cacheLocation: 'localStorage'
  },
  requireAuthOnInitialize: true,
  router: router
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');