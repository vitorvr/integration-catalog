import Vue from 'vue';
import Vuex from 'vuex';
import interfaceStore from './modules/interface';
import applicationStore from './modules/application';
import deploylocationStore from './modules/deploylocation';
import user from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    interfaceStore,
    applicationStore,
    deploylocationStore,
    user
  }
});
