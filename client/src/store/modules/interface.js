import axios from 'axios';

const baseUrl = '/api/interfaces/';

export default {
  state: {
    interfaces: [],
    currentInterface: {}
  },
  mutations: {
    SET_INTERFACES: (state, payload) => {
      state.interfaces = payload;
    },
    SET_CURRENT_INTERFACE: (state, payload) => {
      state.currentInterface = payload;
    },
    CLEAR_CURRENT_INTERFACE: state => {
      state.currentInterface = {};
    }
  },
  actions: {
    fetchInterfaces: async context => {
      let response = await axios.get(baseUrl);
      context.commit('SET_INTERFACES', response.data);
    },
    setCurrentInterface: (context, payload) => {
      context.commit('SET_CURRENT_INTERFACE', payload);
    },
    updateInterface: async (context, payload) => {
      return await axios.put(`${baseUrl}${payload._id}`, payload);
    },
    addInterface: async (context, payload) => {
      return await axios.post(baseUrl, payload);
    },
    deleteInterface: async (context, payload) => {
      await axios.delete(`${baseUrl}${payload._id}`);
    },
    clearCurrentInterface: async context => {
      context.commit('CLEAR_CURRENT_INTERFACE');
    }
  },
  getters: {
    getInterfaces: state => {
      return state.interfaces;
    },
    getCurrentInterface: state => {
      return state.currentInterface;
    }
  }
};
