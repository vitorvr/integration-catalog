import axios from 'axios';

const baseUrl = '/api/applications/';

export default {
  state: {
    applications: [],
    currentApplication: {}
  },
  mutations: {
    SET_APPLICATIONS: (state, payload) => {
      state.applications = payload;
    },
    SET_CURRENT_APPLICATION: (state, payload) => {
      state.currentApplication = payload;
    },
    CLEAR_CURRENT_APPLICATION: state => {
      state.currentApplication = {};
    }
  },
  actions: {
    fetchApplications: async context => {
      let response = await axios.get(baseUrl);
      context.commit('SET_APPLICATIONS', response.data);
    },
    setCurrentApplication: (context, payload) => {
      context.commit('SET_CURRENT_APPLICATION', payload);
    },
    updateApplication: async (context, payload) => {
      return await axios.put(`${baseUrl}${payload._id}`, payload);
    },
    addApplication: async (context, payload) => {
      return await axios.post(baseUrl, payload);
    },
    deleteApplication: async (context, payload) => {
      await axios.delete(`${baseUrl}${payload._id}`);
    },
    clearCurrentApplication: async context => {
      context.commit('CLEAR_CURRENT_APPLICATION');
    }
  },
  getters: {
    getApplications: state => {
      return state.applications;
    },
    getCurrentApplication: state => {
      return state.currentApplication;
    }
  }
};