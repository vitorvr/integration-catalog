import axios from 'axios';

const baseUrl = '/api/deploylocation/';

export default {
  state: {
    locations: []
  },
  mutations: {
    SET_LOCATIONS: (state, payload) => {
      state.locations = payload;
    }
  },
  actions: {
    fetchDeployLocations: async context => {
      let response = await axios.get(baseUrl);
      context.commit('SET_LOCATIONS', response.data);
    }
  },
  getters: {
    getDeployLocations: state => {
      return state.locations;
    }
  }
};
