export default {
  state: {
    user: {}
  },
  mutations: {
    SET_USER: (state, payload) => {
      state.user = payload;
    }
  },
  actions: {
    setUser: (context, payload) => {
      context.commit('SET_LOCATIONS', payload);
    }
  },
  getters: {
    getUser: state => {
      return state.user;
    }
  }
};
