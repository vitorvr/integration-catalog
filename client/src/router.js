import Vue from 'vue';
import Router from 'vue-router';
import Interfaces from './views/Interfaces/Interfaces.vue';
import Interface from './views/Interfaces/Interface.vue';

import Applications from './views/Applications/Applications.vue';
import Application from './views/Applications/Application.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/interfaces', component: Interfaces },
    { path: '/interfaces/interface', component: Interface },
    { path: '/applications', component: Applications },
    { path: '/applications/application', component: Application }
  ]
});
